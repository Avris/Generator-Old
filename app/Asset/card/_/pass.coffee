$('section[data-card=__pass]').card
    generator: (params, options) ->
        pos = for set, el of options
            if $(el).is(':checked') then (if set == 'local' then M.l('card._.pass.option.localValue') else sets[set]) else null
        return pos.join('').randomize parseInt options.passlength.val()
