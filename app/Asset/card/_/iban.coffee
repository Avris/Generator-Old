$('section[data-card=__iban]').card
    validator: (val, params) ->
        val = val.replace(/\s/g,'')
        unless val.match /^\D{2}\d+$/g
            return [false, M.l('validator.format')]
        code = val[0..1].toUpperCase()
        if params[code] is undefined
            return [false, M.l('validator.countryCode')]
        if val.length != params[code]
            return [false, M.l('validator.length')]
        val = val.substr(4) + numerify(code[0]) + numerify(code[1]) + val.substr(2, 2)
        if val.bigModulo(97) != 1
            return [false, M.l('validator.checksum')]
        return [true, "#{M.l('validator.valid')} (#{M.l('country.' + code)})"]
    generator: (params, options) ->
        code = options.country.val()
        unless code
            code = Object.keys(params).randomElement()
        nr = sets.numbers.randomize(params[code] - 4)
        nrBig = nr + numerify(code[0]) + numerify(code[1]) + "00"
        modulo = nrBig.bigModulo(97)
        sk = (97 - modulo + 1) % 97
        ret = code + sk.toString().pad() + nr
        return ret.replace(/\S{4}/g, (m) -> m + ' ').trim()
    params: {"AD":24,"AE":23,"AL":28,"AT":20,"AZ":28,"BA":20,"BE":16,"BG":22,"BH":22,"BR":29,"CH":21,"CR":21,"CY":28,"CZ":24,"DE":22,"DK":18,"DO":28,"EE":20,"ES":24,"FI":18,"FR":27,"FO":18,"GB":22,"GE":22,"GI":23,"GL":18,"GR":27,"GT":28,"HR":21,"HU":28,"IE":22,"IL":23,"IS":26,"IT":27,"JO":30,"KW":30,"KZ":20,"LB":28,"LI":21,"LT":20,"LU":20,"LV":21,"MC":27,"MD":24,"ME":22,"MK":19,"MR":27,"MT":31,"MU":30,"NL":18,"NO":15,"PK":24,"PL":28,"PT":25,"QA":29,"RO":24,"RS":22,"SA":24,"SE":24,"SI":19,"SK":24,"SM":27,"TN":24,"TR":26,"VG":24}
    constructor: (options, params) ->
        countries = Object.keys(params).map (v) ->
            {id: v, text: M.l('country.' + v)}
        countries.sort (a, b) ->
            return a.text.localeCompare(b.text)
        template = (country) ->
            if country.id then $ "<span><span class='flag-#{country.id}'/> #{country.text}</span>" else country.text
        options.country.select2(
            placeholder: M.l('card._.iban.option.allCountries')
            data: countries
            allowClear: true
            width: '100%'
            templateResult: template
            templateSelection: template
        ).val(M.locale.slice(3)).trigger('change')
