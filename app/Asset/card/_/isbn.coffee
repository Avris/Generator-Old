$('section[data-card=__isbn]').card
    validator: (val, params) ->
        val = val.replace(/[- ]/g, '')
        matchingCountries = []
        if val.length == 10
            return [false, M.l('validator.format')] unless val.match /^\d{9}(\d|X)$/g
            cs = (params.sums[10].timesArray(val[0..8]).sum() % 11)
            givenCs = (if val[9] == 'X' then 10 else parseInt(val[9]))
            return [false, M.l('validator.checksum')] unless cs == givenCs
            for code, bcodes of params.countries
                for bcode in bcodes
                    continue if typeof bcode == 'string'
                    bcode += ''
                    if val.substr(0, bcode.length) == bcode and matchingCountries.indexOf(code) == -1
                        matchingCountries.push(code)
        else if val.length == 13
            return [false, M.l('validator.format')] unless val.match /^97(8|9)\d{10}$/g
            cs = (10 - (params.sums[13].timesArray(val[0..11]).sum() % 10)) % 10
            return [false, M.l('validator.checksum')] unless cs  == parseInt(val[12])
            for code, bcodes of params.countries
                for bcode in bcodes
                    bcode = if typeof bcode == 'string' then bcode.replace(/-/g, '') else '978' + bcode
                    if val.substr(0, bcode.length) == bcode and matchingCountries.indexOf(code) == -1
                        matchingCountries.push(code)
        else
            return [false, M.l('validator.format')]

        countriesString = if matchingCountries.length then ((M.l('country.' + ccode)) for ccode in matchingCountries).join(', ') else M.l('validator.unknownCountry')

        return [true, "#{M.l('validator.valid')} (#{countriesString})"]
    generator: (params, options) ->
        code = options.country.val() or Object.keys(params.countries).randomElement()
        bcodes = params.countries[code]
        bcode = bcodes.randomElement()

        if options.isbn10.is(':checked')
            bcode = bcodes[0] if typeof bcode == 'string'
            nr = sets.numbers.randomize(9 - (bcode + '').length)
            cs = (params.sums[10].timesArray(bcode + nr).sum() % 11)
            cs = if cs == 10 then 'X' else cs
            return "#{bcode}-#{nr}-#{cs}"

        [prefix, bcode] = if typeof bcode == 'string' then bcode.split('-') else [978, bcode + '']
        nr = sets.numbers.randomize(9 - bcode.length)
        cs = (10 - (params.sums[13].timesArray(prefix + bcode + nr).sum() % 10)) % 10
        return "#{prefix}-#{bcode}-#{nr}-#{cs}"
    params:
        sums: {10: [1,2,3,4,5,6,7,8,9], 13: [1,3,1,3,1,3,1,3,1,3,1,3]}
        countries: {"_":[92],"AD":[99913,99920],"AG":[976],"AL":[99927],"AM":[5,99930],"AR":[950,987],"AT":[3],"AU":[0,1],"AW":[99904],"AZ":[5,9952],"BA":[86,9958],"BB":[976],"BD":[984],"BE":[2],"BG":[954],"BH":[99901],"BJ":[99919],"BN":[99917],"BR":[85],"BS":[976],"BT":[99936],"BW":[99912],"BY":[5,985],"BZ":[976],"CA":[0,1,2],"CH":[2,3,88],"CK":[982],"CL":[956],"CM":[9956],"CN":[7],"CO":[958],"CR":[9968,9977],"CU":[959],"CY":[9963],"CZ":[80],"DE":[3],"DK":[87],"DM":[976],"DO":[99935],"DZ":[9961],"EC":[9978],"EE":[5,9985],"EG":[977],"ES":[84],"FI":[951,952],"FJ":[982],"FO":[99918],"FR":[2,"979-10"],"GB":[0,1],"GD":[976],"GE":[5,99928],"GH":[9964,9988],"GM":[9983],"GR":[960],"GT":[99922],"GY":[976],"HK":[962],"HN":[99926],"HR":[86,953],"HU":[963],"ID":[979],"IE":[0,1],"IL":[965],"IN":[81,93],"IR":[964],"IS":[9979],"IT":[88,"979-12"],"JM":[976],"JO":[9957],"JP":[4],"KE":[9966],"KG":[5,9967],"KI":[982],"KN":[976],"KR":[89,"979-11"],"KZ":[5,9965],"LB":[9953],"LC":[976],"LK":[955],"LS":[99911],"LT":[5,9955,9986],"LU":[2],"LV":[5,9984],"LY":[9959],"MA":[9954,9981],"MD":[5,9975],"MK":[86,9989],"MN":[99929],"MO":[99937],"MS":[976],"MT":[99909,99932],"MU":[99903],"MV":[99915],"MW":[99908],"MX":[968,970],"MY":[967,983],"NA":[99916],"NG":[978],"NI":[99924],"NL":[90,99904],"NO":[82],"NP":[99933],"NR":[982],"NU":[982],"NZ":[0,1],"PA":[9962],"PG":[9980],"PH":[971],"PK":[969],"PL":[83],"PR":[0,1],"PT":[972],"PY":[99925],"QA":[99921],"RO":[973],"RU":[5],"SA":[9960],"SB":[982],"SC":[99931],"SE":[91],"SG":[981,9971],"SI":[86,961],"SK":[80],"SL":[99910],"SR":[99914],"SV":[99923],"SY":[9972],"SZ":[0,1],"TH":[974],"TJ":[5],"TK":[982],"TM":[5],"TN":[9973],"TO":[982],"TR":[975],"TT":[976],"TV":[982],"TW":[957,986],"TZ":[9976,9987],"UA":[5,966],"UG":[9970],"US":[0,1],"UY":[9974],"UZ":[5],"VC":[976],"VE":[980],"VU":[982],"WS":[982],"ZA":[0,1],"ZM":[9982],"ZW":[0,1]}
    constructor: (options, params) ->
        countries = Object.keys(params.countries).map (v) ->
            {id: v, text: M.l('country.' + v)}
        countries.sort (a, b) ->
            return a.text.localeCompare(b.text)
        template = (country) ->
            if country.id then $ "<span><span class='flag-#{country.id}'/> #{country.text}</span>" else country.text
        options.country.select2(
            placeholder: M.l('card._.isbn.option.allCountries')
            data: countries
            allowClear: true
            width: '100%'
            templateResult: template
            templateSelection: template
        ).val(M.locale.slice(3)).trigger('change')
