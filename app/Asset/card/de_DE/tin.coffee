$('section[data-card=de_DE_tin]').card
    validator: (val, f) ->
        val = val.replace(/[- ]/g,'')
        return [false, M.l('validator.format')] unless val.match /^\d{11}$/g
        return [false, M.l('validator.format')] unless val[0] != '0'
        digits = val.substr(0, 10).split('')
        return [false, M.l('validator.numberOfUniqueDigits')] unless f.checkNumberOfUnique(digits)

        return [false, M.l('validator.checksum')] if f.calculateChecksum(digits) != parseInt(val[10])
        return [true, M.l('validator.valid')]
    generator: (f) ->
        rand = ''
        while rand[0] == '0' or !f.checkNumberOfUnique(rand.split(''))
            rand = sets.numbers.randomize(10)
        return rand + f.calculateChecksum(rand.split(''))
    params:
        calculateChecksum: (digits) ->
            product = 10
            for digit in digits
                sum = (parseInt(digit) + product) % 10;
                sum = 10 if sum == 0
                product = (sum * 2) % 11
            return (11 - product) % 10
        checkNumberOfUnique: (digits) ->
            uniqueDigits = digits.unique().length
            return uniqueDigits == 8 or uniqueDigits == 9
