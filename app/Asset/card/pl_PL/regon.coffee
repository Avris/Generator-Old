$('section[data-card=pl_PL_regon]').card
    validator: (val, params) ->
        return [false, M.l('validator.format')] unless val.match /^(\d{9}|\d{14})$/g
        return [false, M.l('validator.checksum')] unless params[0].timesArray(val[0..7]).sum() % 11 % 10 == parseInt(val[8])
        if val.length == 14
            return [false, M.l('validator.checksum')] unless params[1].timesArray(val[0..12]).sum() % 11 % 10 == parseInt(val[13])
        return [true, M.l('validator.valid')]
    generator: (params, options) ->
        nr = sets.numbers.randomize(8)
        nr += (params[0].timesArray(nr).sum() % 11 % 10)
        if options.regon14.is(':checked')
            nr += sets.numbers.randomize(4)
            nr += (params[1].timesArray(nr).sum() % 11 % 10)
        return nr
    params: [[8,9,2,3,4,5,6,7],[2,4,8,5,0,9,7,3,6,1,2,4,8]]
