$('section[data-card=pl_PL_id]').card
    validator: (val, params) ->
        return [false, M.l('validator.format')] unless val.match /^[A-Z]{3}\d{6}$/g
        return [false, M.l('validator.checksum')] unless params.timesArray(val).sum() % 10 == parseInt(val[3])
        return [true, M.l('validator.valid')]
    generator: (params) ->
        p1 = 'A' + sets.upper.randomize(2)
        p2 = sets.numbers.randomize(5)
        sk = params.timesArray(p1+'0'+p2).sum()
        return p1 + (sk % 10) + p2
    params: [7,3,1,0,7,3,1,7,3]
