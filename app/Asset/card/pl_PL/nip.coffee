$('section[data-card=pl_PL_nip]').card
    validator: (val, params) ->
        val = val.replace(/[- ]/g,'')
        return [false, M.l('validator.format')] unless val.match /^\d{10}$/g
        sk = params.timesArray(val[0..8]).sum() % 11
        return [false, M.l('validator.checksum')] if val == 10 || sk != parseInt(val[9])
        return [true, M.l('validator.valid')]
    generator: (params) ->
        sk = 10
        while (sk % 11 == 10)
            nr = sets.numbers.randomize(9)
            sk = params.timesArray(nr).sum()
        return nr + (sk % 11)
    params: [6,5,7,2,3,4,5,6,7]