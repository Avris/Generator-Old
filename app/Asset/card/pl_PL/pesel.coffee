$('section[data-card=pl_PL_pesel]').card
    validator: (val, params) ->
        unless val.match /^\d{11}$/g
            return [false, M.l('validator.format')]
        [y, m, d, c] = [parseInt(val[0..1]), parseInt(val[2..3]), parseInt(val[4..5]), 19]
        switch
            when m > 80      then m-=80; c=18
            when 20 < m < 40 then m-=20; c=20
            when 40 < m < 60 then m-=40; c=21
            when 60 < m < 80 then m-=60; c=22
        if m > 12 or d > 31 or m == 0 or d == 0
            return [false, M.l('validator.birthdate')]
        unless (10 - (params.timesArray(val[0..9]).sum() % 10)) % 10 is parseInt val[10]
            return [false, M.l('validator.checksum')]

        date = "#{d.toString().pad()}.#{m.toString().pad()}.#{c}#{y.toString().pad()}"
        gender = M.l('card.pl_PL.pesel.option.' + (if val[9] % 2 then 'male' else 'female'))

        t = new Date()
        age = t.getFullYear() - parseInt("#{c}#{y.toString().pad()}")
        if t.getMonth()+1 < m or (t.getMonth()+1 == m and t.getDate() < d) then age--

        return [true, "#{M.l('validator.valid')}. #{date}, #{age} #{age.decline('rok', 'lata', 'lat')}, #{gender}"]
    generator: (params, options) ->
        [dy, dm, dd] = options.birthdate.data('date') ? [undefined, undefined, undefined]
        [y, m, d] = [dy ? Math.randRange(1900, 120), dm ? Math.randRange(1, 12), dd ? Math.randRange(1, 29)]

        m += 80 if y < 1900
        m += 20 if 2000 <= y < 2100
        m += 40 if 2100 <= y < 2200
        m += 60 if 2200 <= y < 2300
        t = Math.randRange(0, 1000)

        gender = if options.genderM.is(':checked') then 'm' else ''
        gender += if options.genderK.is(':checked') then 'k' else ''

        g = if gender == 'mk' or gender == '' then Math.randRange(0,10) else 2*Math.randRange(0, 5) + if gender == 'm' then 1 else 0
        nr = y.toString()[2..3] + m.toString().pad() + d.toString().pad() + t.toString().pad(3) + g
        sk = (10 - (params.timesArray(nr).sum() % 10)) % 10
        return nr+sk
    params: [1,3,7,9,1,3,7,9,1,3]
    constructor: (options) ->
        options.birthdate.on 'change keyup', ->
            t = $(this)
            date = t.val()
            [y,m,d] = [undefined, undefined, undefined]
            switch
                when date == '' then 0
                when date.match /^\d{4}$/g              then y = parseInt date
                when date.match /^\d{4}-\d{2}$/g        then [y,m] = [parseInt(date[0..3]), parseInt(date[5..6])]
                when date.match /^\d{4}-\d{2}-\d{2}$/g  then [y,m,d] = [parseInt(date[0..3]), parseInt(date[5..6]), parseInt(date[8..9])]
                else incorrect = true
            t.data('date', [y,m,d])
            t.validate not (incorrect? || y < 1900 || y > 2299 || m < 0 || m > 12 || d < 0 || d > 31)
