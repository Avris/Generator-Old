Math.randRange = (start, count) -> Math.floor(Math.random() * count) + start
@numerify = (n) -> if isNaN(parseInt(n)) then n.charCodeAt(0)-55 else parseInt(n)

String::randomize = (n) -> out = (this.charAt Math.floor Math.random() * this.length for [1..n]).join('')
String::multiply = (n) -> (this for [1..n]).join('')
String::pad = (n = 2) -> if this.length < n then '0'.multiply(n-this.length) + this else this

Array::timesArray = (arr) -> parseInt(this[i]) * numerify(el) for el, i in arr
Array::sum = -> this.reduce (t, s) -> t + parseInt s
Array::randomElement = -> this[Math.floor(Math.random()*this.length)]
Array::unique = -> this.filter (value, index, self) -> self.indexOf(value) == index
@isArray = Array.isArray || (value) -> return {}.toString.call(value) is '[object Array]'

Number::decline = (nominativSing, nominativ, genitiv) ->
  num = Math.abs(this);
  return nominativSing if num == 1
  ones = num % 10;
  tens = (num / 10) % 10;
  if tens != 1 and ones >= 2 and ones <= 4 then nominativ else genitiv

String::bigModulo = (divisor) ->
  partLength = 10
  divident = this
  while (divident.length > partLength)
    part = divident.substring(0, partLength)
    divident = (part % divisor) +  divident.substring(partLength)
  divident % divisor

@sets =
  lower: 'abcdefghijklmnopqrstuvwxyz'
  upper: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  numbers: '0123456789'
  symbols: '`~!@#$%^&*()_-+=[]{};:?,.<>'
