$.fn.validate = (valid, message = '') -> this.each ->
    t = $(this)
    l = t.parent().find('label')
    if not t.val() then return t.removeClass('valid invalid')
    if valid
        t.addClass('valid').removeClass('invalid')
        l.attr('data-success', message).attr('data-error', '')
    else
        t.removeClass('valid').addClass('invalid')
        l.attr('data-success', '').attr('data-error', message)


$.fn.card = (opt) -> this.each ->
    opt = $.extend({}, {
        validator: -> true
        generator: -> ''
        params: []
    }, opt);

    t = $(this)
    field = t.find('.card-output')
    generateBtn = t.find('.btn-generate')

    options = {}
    t.find('[data-option]')
        .keyup (e) -> generateBtn.click() if e.keyCode == 13
        .each (i, el) -> el = $(el); options[el.data('option')] = el

    field.on 'change keyup', (e) =>
        res = opt.validator.apply(@, [field.val(), opt.params])
        res = [res, undefined] unless isArray res
        field.validate(res[0], res[1])
        generateBtn.click() if e.keyCode == 13

    t.find('.btn-generate').click (e) =>
        res = opt.generator.apply(@, [opt.params, options])
        field.val(res).trigger('change')
        if typeof e.originalEvent != 'undefined'
          field.focus().select()

    t.find('.js-toggle-settings').click ->
        t.find('.settings').toggleClass('hidden')
        $(window).trigger('resize')

    if opt.constructor
        opt.constructor.apply(@, [options, opt.params])

$footer = $('footer')
$window = $(window)
$(window).resize ->
    $footer.removeClass('fixed')
    footerBottom = $footer.outerHeight() + $footer.position().top + parseInt($footer.css('margin-top').replace('px', ''))
    $footer.toggleClass('fixed', footerBottom < $window.height())
.trigger('resize')
