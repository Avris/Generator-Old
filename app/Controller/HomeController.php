<?php
namespace App\Controller;

use App\Service\CardsProvider;
use Avris\Micrus\Controller\Controller;

class HomeController extends Controller
{
    public function homeAction()
    {
        return $this->getService('htmlRenderer')
            ->render('app', ['cardsProvider' => $this->getService('cardsProvider')]);
    }
}
