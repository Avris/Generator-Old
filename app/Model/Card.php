<?php
namespace App\Model;

class Card
{
    /** @var string */
    protected $locale;

    /** @var string */
    protected $name;

    /** @var bool */
    protected $hasOptions;

    /**
     * @param string $locale
     * @param string $name
     * @param bool $hasOptions
     */
    public function __construct($locale, $name, $hasOptions = false)
    {
        $this->locale = $locale;
        $this->name = $name;
        $this->hasOptions = $hasOptions;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function hasOptions()
    {
        return $this->hasOptions;
    }

    public function getId()
    {
        return $this->locale . '_' . $this->name;
    }

    public function matchesLocale($locale)
    {
        return $this->locale === '_' || $this->locale === $locale;
    }
}