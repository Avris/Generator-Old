<?php
namespace App\Service;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\ParameterBag;
use Avris\Micrus\Tool\Cache\Cacher;
use Avris\Micrus\Tool\Locale\Locale;
use Avris\Micrus\Tool\Locale\LocaleSet;

class UrlAppLocaleSet extends LocaleSet
{
    /** @var Locale */
    protected $current;

    /** @var array */
    protected $localesMap;

    /** @var string */
    protected $fallbackName;

    /**
     * @param string $name
     * @param string $dir
     * @param Request $request
     * @param string $fallback
     * @param Cacher $cacher
     * @param ParameterBag $locales
     */
    public function __construct($name, $dir, Request $request, $fallback, Cacher $cacher, ParameterBag $locales)
    {
        $this->localesMap = $locales->getArray();
        $this->fallbackName = $fallback;

        parent::__construct($name, $dir, $request, $fallback, $cacher);
    }

    protected function determineCurrent(Request $request)
    {
        $locale = $this->getCurrentFromQuery($request)
            ?: $this->getCurrentFromUrl($request)
            ?: $this->getCurrentFromHeader($request)
            ?: $this->getFallbackName();

        return new Locale($this, $locale);
    }

    protected function getCurrentFromQuery(Request $request)
    {
        if ($fromQuery = $request->getQuery('locale')) {
            if (isset($this->localesMap[$fromQuery])) {
                return $fromQuery;
            }
        }

        return null;
    }

    protected function getCurrentFromUrl(Request $request)
    {
        $fromUrl = ucfirst(substr($request->getCleanUrl(), 1));

        return array_search($fromUrl, $this->localesMap);
    }

    protected function getCurrentFromHeader(Request $request)
    {
        $fromHeader = $request->getHeaders()->getLanguage();

        usort($fromHeader, function ($a, $b) {
            if ($a['quality'] == $b['quality']) { return 0; }
            return $a['quality'] > $b['quality'] ? -1 : 1;
        });

        foreach ($fromHeader as $language) {
            $name = str_replace('-', '_', $language['language']);
            if (strlen($name) == 2) {
                $name = $name . '_' . strtoupper($name);
            }
            if ($name && isset($this->localesMap[$name])) {
                return $name;
            }
        }

        return null;
    }

    protected function getFallbackName()
    {
        return $this->fallbackName;
    }
}
