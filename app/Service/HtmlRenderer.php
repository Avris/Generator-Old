<?php
namespace App\Service;

use Avris\Micrus\Container;
use Avris\Micrus\Controller\Http\Response;
use Avris\Micrus\Tool\Cache\Cacher;

class HtmlRenderer
{
    /** @var Container */
    protected $container;

    /** @var string */
    protected $locale;

    /** @var Cacher */
    protected $cacher;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->cacher = $container->get('cacher');
        $this->locale = (string) $container->get('localizator')->getCurrent();
    }

    public function render($view, $vars = [], $status = 200)
    {
        $key = sprintf('View/%s_%s', $view, $this->locale);

        $html = $this->cacher->cache($key, function() use ($view, $vars) {
            $vars = array_merge([
                '_view' => $view,
                'assets' => $this->container->get('asseticManager')->buildAll(),
            ], $vars);

            return preg_replace(
                ['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s'],
                ['>', '<', ' '],
                $this->container->get('templater')->render($vars)
            );
        });

        return new Response($html, $status);
    }
}
