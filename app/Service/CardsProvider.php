<?php
namespace App\Service;

use App\Model\Card;
use Avris\Micrus\ParameterBag;

class CardsProvider
{
    /** @var ParameterBag */
    protected $cards;

    public function __construct(ParameterBag $cards)
    {
        $this->cards = $cards;
    }

    public function getCards($currentLocale)
    {
        $currentLocale = (string) $currentLocale;

        foreach ($this->cards as $locale => $cards) {
            if ($locale !== $currentLocale && $locale !== '_') {
                continue;
            }

            foreach ($cards as $name => $hasOptions) {
                yield new Card($locale, $name, $hasOptions);
            }
        }
    }
}
