<?php
namespace App\Service;

use Avris\Micrus\Assetic\AsseticExtension;
use Avris\Micrus\ParameterBag;

class LocaleAsseticExtension implements AsseticExtension
{
    /** @var string */
    protected $rootDir;

    /** @var array */
    protected $config;

    /** @var ParameterBag */
    protected $locales;

    /**
     * @param string $rootDir
     * @param ParameterBag $config
     */
    public function __construct($rootDir, ParameterBag $config, ParameterBag $locales)
    {
        $this->rootDir = $rootDir;
        $this->config = $config->getArray();
        $this->locales = $locales;
    }

    public function getSourceDir()
    {
        return $this->rootDir;
    }

    public function getConfig()
    {
        foreach ($this->locales as $key => $name) {
            $this->addLocale($key);
        }

        return $this->config;
    }

    protected function addLocale($locale)
    {
        $original = $this->config['assets']['js'];

        $replacement = [
            ['card/_/*', 'coffee'],
            ['card/' . $locale . '/*', 'coffee'],
        ];

        array_splice($original['inputs'], -1, 0, $replacement);

        $this->config['assets']['js_' . $locale] = $original;
    }
}
