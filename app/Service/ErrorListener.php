<?php
namespace App\Service;

use Avris\Micrus\Exception\Handler\ErrorEvent;
use Avris\Micrus\Exception\Handler\ErrorHandler;

class ErrorListener
{
    /** @var HtmlRenderer */
    protected $renderer;

    /** @var string */
    protected $dev;

    public function __construct(HtmlRenderer $renderer, $env)
    {
        $this->renderer = $renderer;
        $this->env = $env;
    }

    public function errorEvent(ErrorEvent $event)
    {
        if ($this->env == 'dev') { return; }

        $exception = $event->getException();
        $code = ErrorHandler::clearStatusCode($exception->getCode());

        $event->setResponse(
            $this->renderer->render(
                'error',
                ['code' => $code],
                $code
            )
        );
    }
}
