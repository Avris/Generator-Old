<?php
namespace App\Service;

use App\Model\Card;

class AppTwigExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'app';
    }

    public function getFunctions()
    {
        return [
            'cl' => new \Twig_SimpleFunction('cl', function ($context, $field) {
                /** @var Card $card */
                $card = $context['card'];

                return l(sprintf('card.%s.%s.%s', $card->getLocale(), $card->getName(), $field));
            }, ['needs_context' => true]),
        ];
    }
}
