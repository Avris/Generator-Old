<?php
namespace App\Task;

use Avris\Micrus\Console\Task;
use Avris\Micrus\Model\Widget\ChoiceHelper;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocalizeIsbnTask extends Task
{
    protected function configure()
    {
        $this
            ->setName('localize:isbn')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = [];

        foreach (file(__DIR__ . '/isbn.txt') as $line) {
            $pos = strpos($line, ' ');
            $code = substr($line, 0, $pos);

            $countries = $countries = explode(', ', trim(substr($line, $pos)));
            foreach ($countries as $country) {
                if (strlen($country) > 2) {
//                    if (isset(ChoiceHelper::$country[$country])) {
//                        $output->writeln($country . ' = ' . ChoiceHelper::$country[$country] . '?');
//                    }
//                } else {
                    $countryCode = array_search($country, ChoiceHelper::$country);
                    if ($countryCode === false) {
                        $output->writeln('MISSING: ' . $country . ' ('.$code.')');
                        continue;
                    }
                    $country = $countryCode;
                }
                if ($country == 'International') {
                    $country = '';
                }

                $result[$country][] = (int) $code;
            }
        }

        $result['FR'][] = '979-10';
        $result['KR'][] = '979-11';
        $result['IT'][] = '979-12';

        ksort($result);

        dump($result);

        echo json_encode($result);
    }
}
