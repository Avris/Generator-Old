<?php
namespace App\Task;

use Avris\Micrus\Console\Task;
use Avris\Micrus\Model\Widget\ChoiceHelper;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocalizeCountriesTask extends Task
{
    protected static $countries = ['AD','AE','AL','AT','AZ','BA','BE','BG','BH','BR','CH','CR','CY','CZ','DE','DK','DO','EE','ES','FI','FR','FO','GB','GE','GI','GL','GR','GT','HR','HU','IE','IL','IS','IT','JO','KW','KZ','LB','LI','LT','LU','LV','MC','MD','ME','MK','MR','MT','MU','NL','NO','PK','PL','PT','QA','RO','RS','SA','SE','SI','SK','SM','TN','TR','VG'];

    protected function configure()
    {
        $this
            ->setName('localize:countries')
            ->addArgument('language')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xml = simplexml_load_file($this->rootDir . '/run/locales/' . $input->getArgument('language') . '.xml');

        $output->writeln('country:');

        //foreach (static::$countries as $code) {
        foreach (array_keys(ChoiceHelper::$country) as $code) {
            $name = (string) $xml->xpath('//localeDisplayNames/territories/territory[@type="'.$code.'"]')[0];
            $output->writeln('  ' . $code . ': ' . $name);
        }
    }
}
