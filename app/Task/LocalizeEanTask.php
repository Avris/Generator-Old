<?php
namespace App\Task;

use Avris\Micrus\Console\Task;
use Avris\Micrus\Model\Widget\ChoiceHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocalizeIsbnTask extends Task
{
    protected static $special = [
        'Restricted distribution (MO defined)' => '@restricted',
        'Coupons' => '@coupons',
        'Common Currency Coupons' => '@coupons',
        'Bookland' => '@isbn',
        'Serial publications (ISSN)' => '@issn',
        'Refund receipts' => '@refund',
        'GS1 Global Office: Special applications' => '@special',
        'EPCglobal: Special applications' => '@special',
        'GS1 UK: GTIN-8 allocations' => '@special',
        'GS1 Global Office: GTIN-8 allocations' => '@special',
    ];

    protected function configure()
    {
        $this
            ->setName('localize:ean')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = [];

        foreach (file(__DIR__ . '/ean.txt') as $line) {
            $pos = mb_strpos($line, "\t");

            $codes = $this->buildCodesRange(mb_substr($line, 0, $pos));

            $countries = $countries = explode(', ', trim(mb_substr($line, $pos)));
            foreach ($countries as $country) {
                $country = trim($country);
                $countryCode = array_search($country, ChoiceHelper::$country);
                if ($countryCode === false) {
                    if (!isset(self::$special[$country])) {
                        throw new \Exception($country . ' not found!');
                    }
                    $countryCode = self::$special[$country];
                }

                if (!isset($result[$countryCode])) {
                    $result[$countryCode] = [];
                }

                $result[$countryCode] = array_merge($result[$countryCode], $codes);
            }
        }

        uksort($result, function($a, $b) {
            if ($a == $b) { return 0; }
            if (substr($a, 0, 1) == '@') { return 1; }
            if (substr($b, 0, 1) == '@') { return -1; }
            return $a > $b ? 1 : -1;
        });

        dump($result);

        echo json_encode($result);
    }

    protected function buildCodesRange($string)
    {
        $pos = $pos = mb_strpos($string, '–');

        if ($pos === false) {
            return [(int) trim($string)];
        }

        return range(
            (int) trim(mb_substr($string, 0, $pos)),
            (int) trim(mb_substr($string, $pos + 1))
        );
    }
}
